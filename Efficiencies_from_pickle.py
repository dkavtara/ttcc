import hist
from hist import Hist
import pickle
import matplotlib.pyplot as plt
import numpy as np

ttcc_location_Merge = "/user/dkavtara/columnflow/ttcc/data/cf_store/analysis_ttcc/cf.MergeHistograms/run2_2018_nano_v9/tt_dl/nominal/calib__example/sel__Reco_selection/prod__Reco_production/Jan1/"
# ttcc_location = "/user/dkavtara/columnflow/ttcc/data/cf_store/analysis_ttcc/cf.CreateHistograms/run2_2018_nano_v9/tt_dl/nominal/calib__default/sel__Gen_selection/prod__Gen_production/Gen2/"
# h = Hist(hist.axis.Regular(bins=10,start=0,stop=1,name="x"))

with open(ttcc_location_Merge+"hist__bJet.btagDeepFlavBpC-bJet.btagDeepFlavBvC.pickle", "rb") as f:
    h2 = pickle.load(f)

print(h2.view().value.shape)
inc = h2.view().value[0,0,0,:]
inc /= inc.sum()
print(inc.shape)

tags = ["b","c","l"]

for tag in tags:
    with open(ttcc_location_Merge+f"hist__"+tag+"Jet.btagDeepFlavBpC-" + tag + "Jet.btagDeepFlavBvC.pickle" , "rb") as f:
        h2 = pickle.load(f)
        inc = h2.view().value[0,0,0,:]
        inc /= inc.sum()
        print(inc.shape)

        eff_B4 = inc[3,7]
        eff_B3 = inc[3,6]
        eff_B2 = inc[3,5]
        eff_B1 = inc[3,4]
        eff_B0 = inc[3,3]
        eff_C2 = inc[3,2]
        eff_C3 = inc[3,1]
        eff_C4 = inc[3,0]
        eff_C1 = inc[2,:].sum()
        eff_C0 = inc[1,:].sum()
        eff_L0 = inc[0,:].sum()
        tagging_effs = {"B4":eff_B4, "B3":eff_B3, "B2":eff_B2, "B1":eff_B1, "B0":eff_B0, "C4":eff_C4, "C3":eff_C3, "C2":eff_C2, "C1":eff_C1, "C0":eff_C0, "L0":eff_L0}

        for key in tagging_effs:
            print(f"{key}: {tagging_effs[key]}")



