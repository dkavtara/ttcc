


def get_wp_dict(events, tagger="btagDeepFlav"):
    p_b = getattr(events.Jet, f"{tagger}B")
    p_cvl = getattr(events.Jet, f"{tagger}CvL")
    p_c = p_cvl * (1 - p_b)
    p_bvc = p_b/(p_b + p_c)
    p_hf = p_b + p_c

    WP_B4 = (p_hf > 0.5) & (p_bvc > 0.99)
    WP_B3 = (p_hf > 0.5) & (p_bvc > 0.96) & (p_bvc <= 0.99)
    WP_B2 = (p_hf > 0.5) & (p_bvc > 0.88) & (p_bvc <= 0.96)
    WP_B1 = (p_hf > 0.5) & (p_bvc > 0.70) & (p_bvc <= 0.88)
    WP_B0 = (p_hf > 0.5) & (p_bvc > 0.40) & (p_bvc <= 0.70)
    WP_C2 = (p_hf > 0.5) & (p_bvc > 0.15) & (p_bvc <= 0.40)
    WP_C3 = (p_hf > 0.5) & (p_bvc > 0.05) & (p_bvc <= 0.15)
    WP_C4 = (p_hf > 0.5) & (p_bvc <= 0.05)

    WP_C1 = (p_hf > 0.2) & (p_hf <= 0.5)
    WP_C0 = (p_hf > 0.1) & (p_hf <= 0.2)
    WP_L0 = (p_hf <= 0.1)
    WP = {
        "WP_B4": WP_B4,
        "WP_B3": WP_B3,
        "WP_B2": WP_B2,
        "WP_B1": WP_B1,
        "WP_B0": WP_B0,
        "WP_C4": WP_C4,
        "WP_C3": WP_C3,
        "WP_C2": WP_C2,
        "WP_C1": WP_C1,
        "WP_C0": WP_C0,
        "WP_L0": WP_L0,
        "hfL": (p_hf > 0.2),
        "hfM": (p_hf > 0.5),
        "bL": (p_hf > 0.5) & (p_bvc > 0.7),
        "bM": (p_hf > 0.5) & (p_bvc > 0.88),
        "bT": (p_hf > 0.5) & (p_bvc > 0.96),
        "cL": (p_hf > 0.5) & (p_bvc < 0.4),
        "cM": (p_hf > 0.5) & (p_bvc < 0.15),
        "cT": (p_hf > 0.5) & (p_bvc < 0.05)
    }
    return WP
