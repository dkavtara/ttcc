from columnflow.production import Producer, producer
from columnflow.production.categories import category_ids
from columnflow.production.normalization import normalization_weights
from columnflow.production.cms.seeds import deterministic_seeds
from columnflow.production.cms.mc_weight import mc_weight
from columnflow.production.cms.muon import muon_weights
from columnflow.selection.util import create_collections_from_masks
from columnflow.selection.util import sorted_indices_from_mask
from columnflow.util import maybe_import
from columnflow.columnar_util import EMPTY_FLOAT, Route, set_ak_column
from ttcc.selection.base_selection import TetraVec
from ttcc.selection.Gen_ttbar_ids import custom_process_ids
from ttcc.production.Gen_production import genjet_features
from ttcc.production.pNet_wp import get_wp_dict

np = maybe_import("numpy")
ak = maybe_import("awkward")

coffea = maybe_import("coffea")
maybe_import("coffea.nanoevents.methods.nanoaod")

@producer(
    uses={
        "Lepton.pt", "Lepton.eta", "Lepton.phi", "Lepton.mass"
    },  
    produces={
        "mll"
    }
)
def lep_features(self: Producer, events: ak.Array, **kwargs) -> ak.Array:
    leptons = events.Lepton
    _lep1 = leptons[:, 0]
    _lep2 = leptons[:, 1]
    mll = (TetraVec(_lep1) + TetraVec(_lep2)).mass 
    events = set_ak_column(events, "mll", mll)
    return events

@producer(
    uses={
        # nano columns
        "Jet.*",
        "bJet.*",
        "cJet.*",
        "lJet.*",
    },
    produces = {
        "Jet.btagDeepFlavC", "Jet.btagDeepFlavBvC", "Jet.btagDeepFlavBpC",       
        "bJet.btagDeepFlavC", "bJet.btagDeepFlavBvC", "bJet.btagDeepFlavBpC",       
        "cJet.btagDeepFlavC", "cJet.btagDeepFlavBvC", "cJet.btagDeepFlavBpC",       
        "lJet.btagDeepFlavC", "lJet.btagDeepFlavBvC", "lJet.btagDeepFlavBpC",       
        "n_jet", "n_hftagL", "n_hftagM", 
        "n_btagL", "n_btagM", "n_btagT",
        "n_ctagL", "n_ctagM", "n_ctagT",
        "Jet_HT", "bJet_HT", "cJet_HT", "lJet_HT"
    }
)
def jet_features(self: Producer, events: ak.Array, **kwargs) -> ak.Array:

    # define tagger values 
    for coll in ["Jet", "bJet", "cJet", "lJet"]:
        jets = getattr(events, coll)
        DeepFlavCvL = getattr(jets, "btagDeepFlavCvL")
        DeepFlavB = getattr(jets, "btagDeepFlavB")
        DeepFlavC = DeepFlavCvL * (1 - DeepFlavB)       
        
        events = set_ak_column(events, f"{coll}.btagDeepFlavC", DeepFlavC)
        events = set_ak_column(events, f"{coll}.btagDeepFlavBvC", DeepFlavB/(DeepFlavB+DeepFlavC))
        events = set_ak_column(events, f"{coll}.btagDeepFlavBpC", DeepFlavC+DeepFlavB)

        # add some event variables
        ht = ak.sum( getattr(jets, "pt"), axis=1 )
        events = set_ak_column(events, f"{coll}_HT", ht)

    # create WP definitions
    WP = get_wp_dict(events, tagger="btagDeepFlav")

    hftag_mask_L = (WP["hfL"])
    hftag_mask_M = (WP["hfM"])
    btag_mask_L = (WP["bL"])
    btag_mask_M = (WP["bM"])
    btag_mask_T = (WP["bT"])

    ctag_mask_L = (WP["cL"])
    ctag_mask_M = (WP["cM"])
    ctag_mask_T = (WP["cT"])

    # definition of jet and tag multiplicities
    n_hftagL = ak.sum(hftag_mask_L, axis=1)
    n_hftagM = ak.sum(hftag_mask_M, axis=1)
    n_btagL = ak.sum(btag_mask_L, axis=1)
    n_btagM = ak.sum(btag_mask_M, axis=1)
    n_btagT = ak.sum(btag_mask_T, axis=1)
    n_ctagL = ak.sum(ctag_mask_L, axis=1)
    n_ctagM = ak.sum(ctag_mask_M, axis=1)
    n_ctagT = ak.sum(ctag_mask_T, axis=1)

    # adding them as columns for later use
    events = set_ak_column(events, "n_jet", ak.num(events.Jet.pt, axis=1))
    events = set_ak_column(events, "n_hftagL", n_hftagL)
    events = set_ak_column(events, "n_hftagM", n_hftagM)
    events = set_ak_column(events, "n_btagL", n_btagL)
    events = set_ak_column(events, "n_btagM", n_btagM)
    events = set_ak_column(events, "n_btagT", n_btagT)
    events = set_ak_column(events, "n_ctagL", n_ctagL)
    events = set_ak_column(events, "n_ctagM", n_ctagM)
    events = set_ak_column(events, "n_ctagT", n_ctagT)

    return events
    


@producer(
    uses={
        jet_features, lep_features,
        genjet_features, 
        custom_process_ids, category_ids, 
        normalization_weights, deterministic_seeds, "Muon.pt", "Muon.eta",
    },
    produces={
        jet_features, lep_features,
        genjet_features, 
        custom_process_ids, category_ids, 
        normalization_weights, deterministic_seeds, muon_weights,
    },
)
def Reco_production(self: Producer, events: ak.Array, **kwargs) -> ak.Array:
    # TODO also add here?
    #events = self[custom_process_ids](events, **kwargs)

    # features
    events = self[jet_features](events, **kwargs)
    events = self[lep_features](events, **kwargs)
    if self.dataset_inst.is_mc:
        events = self[genjet_features](events, **kwargs)

    # category ids
    events = self[category_ids](events, **kwargs)

    # deterministic seeds
    events = self[deterministic_seeds](events, **kwargs)
 
    if self.dataset_inst.is_mc:
        # normalization weights
        events = self[normalization_weights](events, **kwargs)
        # split custom processes afterwards
        # TODO is this the right place? otherwise it didnt work
        events = self[custom_process_ids](events, **kwargs)

        # compute muon weights
        # TODO whats this? 
        muon_mask = (events.Muon.pt >= 30) & (abs(events.Muon.eta) < 2.4)
        events = self[muon_weights](events, muon_mask=muon_mask, **kwargs)

    return events
