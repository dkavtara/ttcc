# coding: utf-8

"""
Column production methods related to higher-level features.
"""


from columnflow.production import Producer, producer
from columnflow.production.categories import category_ids
from columnflow.production.normalization import normalization_weights
from columnflow.production.cms.seeds import deterministic_seeds
from columnflow.production.cms.mc_weight import mc_weight
from columnflow.production.cms.muon import muon_weights
from columnflow.selection.util import create_collections_from_masks
from columnflow.selection.util import sorted_indices_from_mask
from columnflow.util import maybe_import
from columnflow.columnar_util import EMPTY_FLOAT, Route, set_ak_column

np = maybe_import("numpy")
ak = maybe_import("awkward")

coffea = maybe_import("coffea")
maybe_import("coffea.nanoevents.methods.nanoaod")

@producer(
    uses={
        "GenJet.*", "GenbJet.*", "GencJet.*", "GenlJet.*"
    },
    produces={
        "n_genjet",
        "n_genbjet",
        "n_gencjet",
        "n_genljet"
    },
)
def genjet_features(self: Producer, events: ak.Array, **kwargs) -> ak.Array:

    # get jet collections from selctor
    genjet = (events.GenJet)
    genbjet = (events.GenbJet)
    gencjet = (events.GencJet)
    genljet = (events.GenlJet)

    # gen jet counters
    events = set_ak_column(events, "n_genjet", ak.num(genjet.pt, axis=-1))
    events = set_ak_column(events, "n_genbjet", ak.num(genbjet.pt, axis=-1))
    events = set_ak_column(events, "n_gencjet", ak.num(gencjet.pt, axis=-1))
    events = set_ak_column(events, "n_genljet", ak.num(genljet.pt, axis=-1))
    return events


@producer(
    uses={
        mc_weight, category_ids,
        # nano columns
        "Jet.pt",
    },
    produces={
        mc_weight, category_ids,
        # new columns
        "cutflow.jet1_pt",
    },
)
def cutflow_features(
    self: Producer,
    events: ak.Array,
    object_masks: dict[str, dict[str, ak.Array]],
    **kwargs,
) -> ak.Array:
    if self.dataset_inst.is_mc:
        events = self[mc_weight](events, **kwargs)

    # apply object masks and create new collections
    reduced_events = create_collections_from_masks(events, object_masks)

    # create category ids per event and add categories back to the
    events = self[category_ids](reduced_events, target_events=events, **kwargs)

    # add cutflow columns
    events = set_ak_column(
        events,
        "cutflow.jet1_pt",
        Route("Jet.pt[:,0]").apply(events, EMPTY_FLOAT),
    )

    return events

# I've taken out the muon weights, can be done in weights.py production file!
@producer(
    uses={
    },
    produces={
    },
)
def Gen_production(self: Producer, events: ak.Array, **kwargs) -> ak.Array:
    # TODO gen only production

    # mc-only weights
    if self.dataset_inst.is_mc:
        # features
        events = self[Gen_features](events, **kwargs)
        # Need to be careful
        process_id = events.process_id
        # normalization weights
        events = self[normalization_weights](events, **kwargs) 
        muon_mask = (events.Muon.pt >= 30) & (abs(events.Muon.eta) < 2.4)
        events = self[muon_weights](events, muon_mask=muon_mask, **kwargs)
        #re-apply process_ids
        events = set_ak_column(events, "process_id", process_id,value_type=np.int32)
    else:
        events = self[features](events, **kwargs)

    events = self[category_ids](events, **kwargs)
    # deterministic seeds
    events = self[deterministic_seeds](events, **kwargs)

    return events
