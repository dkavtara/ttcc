# coding: utf-8

"""
Exemplary selection methods.
"""

from columnflow.categorization import Categorizer, categorizer
from columnflow.config_util import create_category_combinations
from columnflow.util import maybe_import
from columnflow.columnar_util import Route

ak = maybe_import("awkward")

#Definition of categories:
    #                lowest digit
    #                           |
    # +---+---+---+---+---+---+---+
    # | D | D | B | B | X | T | C |
    # +---+---+---+---+---+---+---+
# B = baseline (1:4j, 2:2j) 
# C = channel (1: ee, 2: emu, 3: mumu)
# T = Tagging (1: ttbb, 2: ttbj, 3: ttcc, 4: ttcj, 5: ttll, 6: ttother)
# TODO: Add more categories
# categorizer functions used by categories definitions
#


@categorizer(uses={"n_lep", "n_jet"})
def cat_2j(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    mask = (events.n_jet >= 2) & (events.n_lep == 2)
    return events, mask

@categorizer(uses={"n_lep", "n_jet"})
def cat_4j(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    mask = (events.n_jet >= 4) & (events.n_lep == 2)
    return events, mask

@categorizer(uses={"event", "channel_id"})
def cat_ee(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    ch_id = self.config_inst.get_channel("ee").id
    mask = events.channel_id == ch_id
    return events, mask

@categorizer(uses={"event", "channel_id"})
def cat_emu(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    ch_id = self.config_inst.get_channel("emu").id
    mask = events.channel_id == ch_id
    return events, mask

@categorizer(uses={"event", "channel_id"})
def cat_mumu(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    ch_id = self.config_inst.get_channel("mumu").id
    mask = events.channel_id == ch_id
    return events, mask

#----------------------------------
#Reco level Categories
#----------------------------------
@categorizer(uses={"n_lep", "n_hftagL", "n_hftagM", "n_jet"})
def cat_4hf(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    mask = (events.n_jet >= 4) & (events.n_hftagL >= 4) & (events.n_lep == 2)
    return events, mask

@categorizer(uses={"n_lep", "n_btagL", "n_btagM", "n_btagT", "n_jet"})
def cat_4b(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    mask = (events.n_jet >= 4) & (events.n_btagL >= 4) & (events.n_lep == 2)
    return events, mask

@categorizer(uses={"n_lep", "n_btagL", "n_btagM", "n_btagT", "n_jet"})
def cat_3b(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    mask = (events.n_jet >= 4) & (events.n_btagL == 3) & (events.n_lep == 2)
    return events, mask

@categorizer(uses={"n_lep", "n_btagL", "n_btagM", "n_btagT", "n_ctagL", "n_ctagM", "n_ctagT", "n_jet"})
def cat_2c(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    mask = (events.n_jet >= 4) & (events.n_btagL == 2) & (events.n_ctagL >= 2) & (events.n_lep == 2)
    return events, mask

@categorizer(uses={"n_lep", "n_btagL", "n_btagM", "n_btagT", "n_ctagL", "n_ctagM", "n_ctagT", "n_jet"})
def cat_1c(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    mask = (events.n_jet >= 4) & (events.n_btagL == 2) & (events.n_ctagL == 1) & (events.n_lep == 2)
    return events, mask

#----------------------------------
#Gen level Categories
#----------------------------------

# @categorizer(uses={"event"})
# def Gen_incl(self:Categorizer, events:ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
#     return events, ak.ones_like(events.event) == 1
# TODO clean up
# @categorizer(uses={"Gen_n_BJet"},mc_only=True)
@categorizer(uses={"event"})
def Gen_ttbb(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    #select ony events with more than 4 GenJets
    if self.dataset_inst.is_mc:
        mask = (Route("Gen_n_BJet").apply(events) >= 4)
    else: # if it's data apply the reco selection
        mask = events["n_bjet"] >= 4
    return events, mask

@categorizer(uses={"event"})
def Gen_ttbj(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    if self.dataset_inst.is_mc:
        mask = events.Gen_n_BJet == 3
    else:
        mask = events["n_bjet"] == 3
    return events, mask

@categorizer()
def Gen_ttcc(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    if self.dataset_inst.is_mc:
        mask = (events.Gen_n_BJet == 2) & (events.Gen_n_CJet >= 2)
    else:
        mask = (events["n_bjet"] == 2) & (events["n_cjet"] >= 2)
    return events, mask

@categorizer()
def Gen_ttcj(self:Categorizer,events:ak.Array,**kwargs) -> tuple[ak.Array,ak.Array]:
    if self.dataset_inst.is_mc:
        mask = (events.Gen_n_BJet == 2) & (events.Gen_n_CJet == 1)
    else: 
        mask = (events["n_bjet"] == 2) & (events["n_cjet"] == 1)
    return events, mask

@categorizer()
def Gen_ttll(self:Categorizer,events:ak.Array,**kwargs) -> tuple[ak.Array,ak.Array]:
    if self.dataset_inst.is_mc:
        mask = (events.Gen_n_BJet == 2) & (events.Gen_n_CJet == 0) & (events.Gen_n_LightJet >= 0)
    else:
        mask = (events["n_bjet"] == 2) & (events["n_cjet"] == 0) & (events["n_ljet"] >= 0)
    return events, mask

@categorizer()
def Gen_ttother(self:Categorizer,events:ak.Array,**kwargs) -> tuple[ak.Array,ak.Array]:
    if self.dataset_inst.is_mc:
        mask = events.Gen_n_BJet <= 1
    else:
        mask = ak.ones_like(events.event) == 1
    return events, mask

