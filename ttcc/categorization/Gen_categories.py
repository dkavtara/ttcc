# coding: utf-8

"""
Exemplary selection methods.
"""

from columnflow.categorization import Categorizer, categorizer
from columnflow.config_util import create_category_combinations
from columnflow.util import maybe_import
from columnflow.columnar_util import Route
#from ttcc.selection.Gen_features import Genjet_masks, get_Genlep_mask
ak = maybe_import("awkward")

# Gen categories
# Baseline is >=4genjets and ==2 gen leptons

'''
@categorizer(uses={"event"})
def cat_incl(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    # fully inclusive selection
    return events, ak.ones_like(events.event) == 1

@categorizer(uses={"event"})
def Gen_G4b(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    #Baseline + >=4 GenBJets
    mask = (Route("Gen_n_BJet").apply(events) >= 4)
    return events, mask

@categorizer(uses={"event"})
def Gen_G3b(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    #Baseline + >=3 GenBJets
    mask = (Route("Gen_n_BJet").apply(events) >= 3)
    return events, mask

@categorizer(uses={"event"})
def Gen_G2b(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    #Baseline + >=2 GenBJets
    mask = (Route("Gen_n_BJet").apply(events) >= 2)
    return events, mask

@categorizer(uses={"event"})
def Gen_G2c(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    #Baseline + >=2 GenCJets
    mask = (Route("Gen_n_CJet").apply(events) >= 2)
    return events, mask

@categorizer(uses={"event"})
def Gen_G2bG2c(self: Categorizer, events: ak.Array, **kwargs) -> tuple[ak.Array, ak.Array]:
    #Baseline + >=2 GenBJets + >=2 GenCJets
    mask = (Route("Gen_n_BJet").apply(events) >= 2) & (Route("Gen_n_CJet").apply(events) >= 2)
    return events, mask
'''
