# coding: utf-8

"""
Custom base tasks.
"""

from columnflow.tasks.framework.base import BaseTask


class TTCCTask(BaseTask):

    task_namespace = "ttcc"
