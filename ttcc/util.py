# coding: utf-8

"""
Collection of helpers
"""

from __future__ import annotations

from typing import Hashable, Iterable, Callable

import law

from columnflow.util import maybe_import

np = maybe_import("numpy")

def call_once_on_config(include_hash=False):
    """
    Parametrized decorator to ensure that function *func* is only called once for the config *config*
    """
    def outer(func):
        def inner(config, *args, **kwargs):
            tag = f"{func.__name__}_called"
            if include_hash:
                tag += f"_{func.__hash__()}"

            if config.has_tag(tag):
                return

            config.add_tag(tag)
            return func(config, *args, **kwargs)
        return inner
    return outer


def four_vec(
    collections: str | Iterable[str],
    columns: str | Iterable[str] | None = None,
    skip_defaults: bool = False,
) -> set[str]:
    """
    Helper to quickly get a set of 4-vector component string for all collections in *collections*.
    Additional columns can be added wih the optional *columns* parameter.

    Example:

    .. code-block:: python

    four_vec("Jet", "jetId")
    # -> {"Jet.pt", "Jet.eta", "Jet.phi", "Jet.mass", "Jet.jetId"}

    four_vec({"Electron", "Muon"})
    # -> {
            "Electron.pt", "Electron.eta", "Electron.phi", "Electron.mass",
            "Muon.pt", "Muon.eta", "Muon.phi", "Muon.mass",
        }
    """
    # make sure *collections* is a set
    collections = law.util.make_set(collections)

    # transform *columns* to a set and add the default 4-vector components
    columns = law.util.make_set(columns) if columns else set()
    default_columns = {"pt", "eta", "phi", "mass"}
    if not skip_defaults:
        columns |= default_columns

    outp = set(
        f"{obj}.{var}"
        for obj in collections
        for var in columns
    )

    # manually remove MET eta and mass
    outp = outp.difference({"MET.eta", "MET.mass"})

    return outp


# coding: utf-8
"""
Analysis-wide utility functions
"""
import math


def iter_chunks(*arrays, max_chunk_size):
    """
    Iterate over one or more arrays in chunks of at most `max_chunk_size`.
    If `max_chunked_size` is negative or zero, no chunking is done.
    """

    # validate input sizes
    size = len(arrays[0])
    if any(len(arr) != size for arr in arrays[1:]):
        lengths_str = ", ".join(str(len(arr)) for arr in arrays)
        raise ValueError(f"array length mismatch: {lengths_str}")

    # compute number of chunks
    n_chunks = 1
    if max_chunk_size > 0:
        n_chunks = int(math.ceil(size / max_chunk_size))

    # if no chunking is needed, yield the arrays directly
    if n_chunks == 1:
        yield arrays
        return

    # loop over chunks
    for i_chunk in range(n_chunks):
        end = min(size, (i_chunk + 1) * max_chunk_size)
        slc = slice(i_chunk * max_chunk_size, end)

        yield tuple(a[slc] for a in arrays)
