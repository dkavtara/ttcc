    
from columnflow.columnar_util import EMPTY_FLOAT

def add_default_variables(cfg):
    variables = []
    cfg.add_variable(
        name="event",
        expression="event",
        binning=(1, 0.0, 1.0e9),
        x_title="Event number",
        discrete_x=True,
    )
    cfg.add_variable(
        name="process_id",
        expression="process_id",
        binning=(101,1200,1300),
        x_title="id",
    )
    cfg.add_variable(
        name="run",
        expression="run",
        binning=(1, 100000.0, 500000.0),
        x_title="Run number",
        discrete_x=True,
    )
    cfg.add_variable(
        name="lumi",
        expression="luminosityBlock",
        binning=(1, 0.0, 5000.0),
        x_title="Luminosity block",
        discrete_x=True,
    )

    # weights
    cfg.add_variable(
        name="mc_weight",
        expression="mc_weight",
        binning=(200, -10, 10),
        x_title="MC weight",
    )
    return variables

def add_lep_variables(cfg):
    cfg.add_variable(
        name="mll",
        expression="mll",
        binning=(50,0,200),
        x_title=r"$m_{\ell\ell}$",
        unit="GeV",
        )
    cfg.add_variable(
        name="lep1_pt",
        expression="Lepton.pt[:,0]",
        binning=(50,0,200),
        x_title=r"$p_{T}$ leading lepton",
        unit="GeV"
        )
    cfg.add_variable(
        name="lep2_pt",
        expression="Lepton.pt[:,1]",
        binning=(50,0,200),
        x_title=r"$p_{T}$ sub leading lepton",
        unit="GeV"
        )
    variables = [      
        "mll", "lep1_pt", "lep2_pt"
         ]
    return variables
    


def add_jet_variables(cfg):
    cfg.add_variable(
        name="n_jet",
        expression="n_jet",
        binning=(8, 1.5, 9.5),
        x_title="Number of jets",
        discrete_x=True,
    )
    cfg.add_variable(
        name="n_btagL",
        expression="n_btagL",
        binning=(7,-0.5, 6.5),
        x_title="Number of b-tagged jets (loose WP)",
        discrete_x=True,
        )
    cfg.add_variable(
        name="n_btagM",
        expression="n_btagM",
        binning=(7,-0.5, 6.5),
        x_title="Number of b-tagged jets (medium WP)",
        discrete_x=True,
        )
    cfg.add_variable(
        name="n_btagT",
        expression="n_btagT",
        binning=(7,-0.5, 6.5),
        x_title="Number of b-tagged jets (tight WP)",
        discrete_x=True,
        )
    cfg.add_variable(
        name="n_ctagL",
        expression="n_ctagL",
        binning=(5,-0.5, 4.5),
        x_title="Number of c-tagged jets (loose WP)",
        discrete_x=True,
        )
    cfg.add_variable(
        name="n_ctagM",
        expression="n_ctagM",
        binning=(5,-0.5, 4.5),
        x_title="Number of c-tagged jets (medium WP)",
        discrete_x=True,
        )
    cfg.add_variable(
        name="n_ctagT",
        expression="n_ctagT",
        binning=(5,-0.5, 4.5),
        x_title="Number of c-tagged jets (tight WP)",
        discrete_x=True,
        )


    cfg.add_variable(
        name="jets_pt",
        expression="Jet.pt",
        binning=(40, 0.0, 400.0),
        unit="GeV",
        x_title=r"$p_{T}$ of all jets",
    )
    cfg.add_variable(
        name="jet1_pt",
        expression="Jet.pt[:,0]",
        null_value=EMPTY_FLOAT,
        binning=(40, 0.0, 400.0),
        unit="GeV",
        x_title=r"$p_{T}$ of leading jet",
    )
    cfg.add_variable(
        name="jet1_eta",
        expression="Jet.eta[:,0]",
        null_value=EMPTY_FLOAT,
        binning=(50, -2.5, 2.5),
        x_title=r"$\eta$ of leading jet",
    )
    cfg.add_variable(
        name="jet_ht",
        expression="Jet_HT",
        binning=(40, 0.0, 800.0),
        unit="GeV",
        x_title="HT of jets",
    )
    # B-tagging
    cfg.add_variable(
        name = "jet_bpc_score",
        expression = "Jet.btagDeepFlavBpC",
        binning = (50, 0, 1),
        x_title = "b+c tagging score of all jets",
    )
    variables = [
        "n_jet", "n_btagL", "n_btagM", "n_btagT", 
        "n_ctagL", "n_ctagM", "n_ctagT", "n_hftagL", "n_hftagM",
        "jets_pt", "jet1_pt", "jet1_eta", "jet_ht", "jet_bpc_score"
        ]
    return variables
