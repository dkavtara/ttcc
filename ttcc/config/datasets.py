# configuration of the Run2 ttcc datasets

from __future__ import annotations
import law 
import order as od
import cmsdb.processes as procs
from columnflow.tasks.external import GetDatasetLFNs

def add_ttcc_datasets(config: od.Config, campaign: od.Campaign):

    dataset_names = [
            # "data_mumu_a", ...

            # signal
            "tt_dl_powheg",

            #backgrounds
            #ewk
            # "dy_lep_m50_amcatnlo",
            # # "dy_lept_m50_ht-100to200_madgraph",
            # # "dy_lept_m50_ht-200to400_madgraph",
            # # "dy_lept_m50_ht-400to600_madgraph",
            # # "dy_lept_m50_ht-600to800_madgraph",
            # # "dy_lept_m50_ht-800to1200_madgraph",
            # # "dy_lept_m50_ht-1200to2500_madgraph",
            # # Single top
            # "st_tchannel_t", #st_tchannel is the mother process of these 2
            # "st_tchannel_tbar",
        ]

    # loop over all dataset names and add them to the config
    for dataset_name in dataset_names:
        config.add_dataset(campaign.get_dataset(dataset_name))

def configure_ttcc_datasets(config: od.Config, limit_dataset_files: int | None = None):

    for dataset in config.datasets:
        if limit_dataset_files:
            #apply optional limit on the max. number of files per dataset
            for info in dataset.info.values():
                info.n_files = min(info.n_files, limit_dataset_files)
    
        if dataset.name.startswith("data_egamma"):
            dataset.add_tag("Egamma")
        if dataset.name.startswith("data_mumu"):
            dataset.add_tag("DoubleMuon")

        #needed for custom tt_dl splitting
        '''
        if dataset.name == "tt_dl_powheg":
             dataset.add_process(config.get_process("ttbb"))
             dataset.add_process(config.get_process("ttbj"))
             dataset.add_process(config.get_process("ttcc"))
             dataset.add_process(config.get_process("ttcj"))
             dataset.add_process(config.get_process("ttll"))
             dataset.add_process(config.get_process("ttother"))
        '''

        # for each dataset, select which triggers to require
        # (and which to veto to avoid double counting events
        # in recorded data)
        if dataset.is_data:
            prev_triggers = set()
            for tag, triggers in config.x.trigger_matrix:
                if dataset.has_tag(tag):
                    dataset.x.require_triggers = triggers
                    dataset.x.veto_triggers = prev_triggers
                    break
                prev_triggers = prev_triggers | triggers

        # elif dataset.is_mc:
        #     dataset.x.require_triggers = config.x.all_triggers


         
