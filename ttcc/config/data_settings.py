
from scinum import Number
from columnflow.util import DotDict

def set_lumi(cfg, year):
    # lumi values in inverse pb
    # https://twiki.cern.ch/twiki/bin/view/CMS/LumiRecommendationsRun2?rev=2#Combination_and_correlations
    #  #check hh2bbww config_run2.py
    if year == 2016:
        cfg.x.luminosity = Number(36310, {
            "lumi_13TeV_2016": 0.01j,
            "lumi_13TeV_correlated": 0.006j,
        })
    elif year == 2017:
        cfg.x.luminosity = Number(41480, {
            "lumi_13TeV_2017": 0.02j,
            "lumi_13TeV_1718": 0.006j,
            "lumi_13TeV_correlated": 0.009j,
        })
    else:  # 2018
        cfg.x.luminosity = Number(59830, {
            "lumi_13TeV_2017": 0.015j,
            "lumi_13TeV_1718": 0.002j,
            "lumi_13TeV_correlated": 0.02j,
        })
    return cfg

def set_minbias(cfg):
    # minimum bias cross section in mb (milli) for creating PU weights, values from
    # https://twiki.cern.ch/twiki/bin/view/CMS/PileupJSONFileforData?rev=45#Recommended_cross_section
    cfg.x.minbias_xs = Number(69.2, 0.046j)
    return cfg

def set_triggers(cfg):
    # TODO: wrong triggers
    # trigger paths for muon/electron channels
    cfg.set_aux("triggers", DotDict.wrap({
        "lowpt": {
            "all": {
                "triggers": {
                    "muon": {
                        "IsoMu27",
                    },
                    "electron": {
                        "Ele35_WPTight_Gsf",
                    },
                },
            },
        },
        "highpt": {
            # split by run range
            # (some triggers inactive active for early runs)
            "early": {
                "triggers": {
                    "muon": {
                        "Mu50",
                    },
                    "electron": {
                        "Ele35_WPTight_Gsf",
                    },
                    "photon": {
                        "Photon200",
                    },
                },
                "run_range_max": 299329,
                "mc_trigger_percent": 11.58,
            },
            "late": {
                "triggers": {
                    "muon": {
                        "Mu50",
                        "TkMu100",
                        "OldMu100",
                    },
                    "electron": {
                        "Ele115_CaloIdVT_GsfTrkIdT",
                    },
                    "photon": {
                        "Photon200",
                    },
                },
            },
        },
    }))
    # ensure mc trigger fraction add up to 100%
    cfg.x.triggers.highpt.late.mc_trigger_percent = (
        100. - cfg.x.triggers.highpt.early.mc_trigger_percent
    )
    return cfg

def set_metfilters(cfg):
    # MET filters
    # https://twiki.cern.ch/twiki/bin/view/CMS/MissingETOptionalFiltersRun2?rev=158#2018_2017_data_and_MC_UL
    cfg.set_aux("met_filters", {
        "Flag.goodVertices",
        "Flag.globalSuperTightHalo2016Filter",
        "Flag.HBHENoiseFilter",
        "Flag.HBHENoiseIsoFilter",
        "Flag.EcalDeadCellTriggerPrimitiveFilter",
        "Flag.BadPFMuonFilter",
        "Flag.BadPFMuonDzFilter",
        "Flag.eeBadScFilter",
        "Flag.ecalBadCalibFilter",
    })
    return cfg
