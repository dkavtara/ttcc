from columnflow.config_util import add_shift_aliases
from columnflow.util import DotDict

def add_shifts(cfg, year, campaign):
    corr_postfix = f"{campaign.x.vfp}VFP" if year == 2016 else ""

    # names of muon correction sets and working points
    # (used in the muon producer)
    cfg.x.muon_sf_names = ("NUM_TightRelIso_DEN_TightIDandIPCut", f"{year}_UL")

    # register shifts
    cfg.add_shift(name="nominal", id=0)

    # tune shifts are covered by dedicated, varied datasets, so tag the shift as "disjoint_from_nominal"
    # (this is currently used to decide whether ML evaluations are done on the full shifted dataset)
    cfg.add_shift(name="tune_up", id=1, type="shape", tags={"disjoint_from_nominal"})
    cfg.add_shift(name="tune_down", id=2, type="shape", tags={"disjoint_from_nominal"})

    # fake jet energy correction shift, with aliases flaged as "selection_dependent", i.e. the aliases
    # affect columns that might change the output of the event selection
    cfg.add_shift(name="jec_up", id=20, type="shape")
    cfg.add_shift(name="jec_down", id=21, type="shape")
    add_shift_aliases(
        cfg,
        "jec",
        {
            "Jet.pt": "Jet.pt_{name}",
            "Jet.mass": "Jet.mass_{name}",
            "MET.pt": "MET.pt_{name}",
            "MET.phi": "MET.phi_{name}",
        },
    )

    # event weights due to muon scale factors
    cfg.add_shift(name="mu_up", id=10, type="shape")
    cfg.add_shift(name="mu_down", id=11, type="shape")
    add_shift_aliases(cfg, "mu", {"muon_weight": "muon_weight_{direction}"})


    # JEC uncertainty sources propagated to btag scale factors
     # (names derived from contents in BTV correctionlib file)
    cfg.x.btag_sf_jec_sources = [
            "",  # total
            "Absolute",
            "AbsoluteMPFBias",
            "AbsoluteScale",
            "AbsoluteStat",
            f"Absolute_{year}",
            "BBEC1",
            f"BBEC1_{year}",
            "EC2",
            f"EC2_{year}",
            "FlavorQCD",
            "Fragmentation",
            "HF",
            f"HF_{year}",
            "PileUpDataMC",
            "PileUpPtBB",
            "PileUpPtEC1",
            "PileUpPtEC2",
            "PileUpPtHF",
            "PileUpPtRef",
            "RelativeBal",
            "RelativeFSR",
            "RelativeJEREC1",
            "RelativeJEREC2",
            "RelativeJERHF",
            "RelativePtBB",
            "RelativePtEC1",
            "RelativePtEC2",
            "RelativePtHF",
            "RelativeSample",
            f"RelativeSample_{year}",
            "RelativeStatEC",
            "RelativeStatFSR",
            "RelativeStatHF",
            "SinglePionECAL",
            "SinglePionHCAL",
            "TimePtEta",
    ]


    # b-tag working points
    # https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL16preVFP?rev=6
    # https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL16postVFP?rev=8
    # https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL17?rev=15
    #https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation106XUL17?rev=17
    btag_key = f"2016{campaign.x.vfp}" if year == 2016 else year
    cfg.x.btag_working_points = DotDict.wrap({
        "deepjet": {
            "loose": {"2016pre": 0.0508, "2016post": 0.0480, 2017: 0.0532, 2018: 0.0490}[btag_key],
            "medium": {"2016pre": 0.2598, "2016post": 0.2489, 2017: 0.3040, 2018: 0.2783}[btag_key],
            "tight": {"2016pre": 0.6502, "2016post": 0.6377, 2017: 0.7476, 2018: 0.7100}[btag_key],
            },
        "deepcsv": {
            "loose": {"2016pre": 0.2027, "2016post": 0.1918, 2017: 0.1355, 2018: 0.1208}[btag_key],
            "medium": {"2016pre": 0.6001, "2016post": 0.5847, 2017: 0.4506, 2018: 0.4168}[btag_key],
            "tight": {"2016pre": 0.8819, "2016post": 0.8767, 2017: 0.7738, 2018: 0.7665}[btag_key],
            },
        })

    # https://btv-wiki.docs.cern.ch/ScaleFactors/UL2018/
    cfg.x.cvLtag_working_points = DotDict.wrap({
        "deepjet":{
            "loose":{2018:0.038},
            "medium":{2018:0.099},
            "tight":{2018:0.282},
        }
    })
    cfg.x.cvBtag_working_points = DotDict.wrap({
        "deepjet":{
            "loose":{2018:0.246},
            "medium":{2018:0.325},
            "tight":{2018:0.267},
        }
    })

    cfg.x.btag_sf = ("deepJet_shape", cfg.x.btag_sf_jec_sources)
    # names of electron correction sets and working points
    # (used in the electron_sf producer)
    cfg.x.electron_sf_names = ("UL-Electron-ID-SF", f"{year}{corr_postfix}", "wp80iso")
    cfg.x.muon_sf_names = ("NUM_TightRelIso_DEN_TightIDandIPCut", f"{year}{corr_postfix}_UL")



    # external files
    json_mirror = "/pnfs/iihe/cms/store/user/dkavtara/columnflow/external/toy_analysis_external" #some toy external files
    json_mirror_juvanden ="/pnfs/iihe/cms/store/user/juvanden/public/external" #A lot of thanks to Jules for collecting the external files!

    # /afs/cern.ch/work/m/mrieger/public/mirrors/jsonpog-integration-9ea86c4c
    cfg.x.external_files = DotDict.wrap({
        # jet energy correction
        "jet_jerc": (f"{json_mirror_juvanden}/POG/JME/{year}{corr_postfix}_UL/jet_jerc.json.gz", "v1"),

        # lumi files
        "lumi": {
            # "golden": (f"{json_mirror}/lumi/Cert_Collisions2022_eraE_359022_360331_Golden.json", "v1"),  # noqa for run3 eraE
            "golden": (f"{json_mirror}/lumi/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt", "v1"),
            "normtag": (f"{json_mirror}/lumi/normtag_PHYSICS.json", "v1"),
            # "normtag": (f"{json_mirror}/lumi/normtag_BRIL.json", "v1"),
            # #For run3 2022 https://twiki.cern.ch/twiki/bin/view/CMS/LumiRecommendationsRun3
        },

        # muon scale factors
        "muon_sf": (f"{json_mirror}/POG/MUO/{year}_UL/muon_Z.json.gz", "v1"),  #for 2018/2017
        # "muon_sf": (f"{json_mirror}/POG/MUO/2022_eraE/Cert_Collisions2022_eraE_359022_360331_Muon.json", "v1")
    })

    return cfg
