from columnflow.util import DotDict

keep_columns = DotDict.wrap({
        "cf.MergeSelectionMasks": {
            "mc_weight", "normalization_weight", "process_id",
            "category_ids", "cutflow.*", "channel_id",
            # number of generator particles&Jets
            #"Gen_n_Lepton","Gen_n_Jet","Gen_n_BJet","Gen_n_CJet","Gen_n_LightJet",
            #"Jet.btagDeepFlavB", "Jet.btagDeepFlavCvB", "Jet.btagDeepFlavCvL", "Jet.hadronFlavour", "bJet.*",
        },
        "cf.ReduceEvents": {
        #
        # NanoAOD columns
        #

        # general event info
        "run", "luminosityBlock", "event",

        # weights
        "genWeight",
        "LHEWeight.*",
        "LHEPdfWeight", "LHEScaleWeight",

        # muons
        "Muon.pt", "Muon.eta", "Muon.phi", "Muon.mass",
        "Muon.pfRelIso04_all",
        "VetoMuon.pt", "VetoMuon.eta", "VetoMuon.phi", "VetoMuon.mass",
        # electrons
        "Electron.pt", "Electron.eta", "Electron.phi", "Electron.mass",
        "Electron.deltaEtaSC",
        "Electron.pfRelIso03_all",

        "Lepton.*", 

        "Jet.pt", "Jet.eta", "Jet.phi", "Jet.mass",
        "Jet.rawFactor", "Jet.hadronFlavour",
        "Jet.btagDeepFlavB", "Jet.btagDeepFlavC",
        "Jet.btagDeepFlavCvB", "Jet.btagDeepFlavCvL",

        "n_*", # all of the counting variables

        # truth jet flavor
        "bJet.*", "cJet.*", "lJet.*",

        # generator quantities
        "Generator.*",

        # generator particles
        "GenPart.*",

        # number of generator particles&Jets
        "GenbJet.*", "GencJet.*", "GenlJet.*", "GenJet.*",
        "GenLepton.*","GenLep.*",

        # generator objects
        "GenMET.*",
        "GenJetAK8.*",

        # missing transverse momentum
        "MET.pt", "MET.phi", "MET.significance", "MET.covXX", "MET.covXY", "MET.covYY",

        # number of primary vertices
        "PV.npvs",

        # average number of pileup interactions
        "Pileup.nTrueInt",

        # generic leptons (merger of Muon/Electron)
        "Lepton.*",

        # columns for PlotCutflowVariables
        "cutflow.*",

        # other columns, required by various tasks
        "channel","channel_id", "category_ids", "process_id",
        "deterministic_seed",
        "mc_weight",
        "pt_regime",
        "pu_weight*",
    },
})
