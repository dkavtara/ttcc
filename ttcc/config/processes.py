# Config of ttcc processes

import order as od
from columnflow.config_util import get_root_processes_from_campaign
import cmsdb.constants as const

# add tt_dl processes
def add_ttcc_processes(config: od.Config, campaign: od.Campaign):
    #get all root processes
    procs = get_root_processes_from_campaign(campaign)
    #config.add_process(procs.n.data)
    # config.add_process(procs.n.tt)
    config.add_process(procs.n.tt_dl)   
    #config.add_process(procs.n.dy_lep)
    #config.add_process(procs.n.st_tchannel)
    # tt = config.get_process("tt")
    tt_dl = config.get_process("tt_dl")
    tt_dl_id = config.get_process('tt_dl').id

    # Add sub-processes to split the tt_dl into ttbb,ttbj,ttcc,ttcj,ttll,ttother
    ttbb = tt_dl.add_process(
        name="ttbb",
        id=tt_dl_id + 52,
        label="ttbb",
        color="#e42536",
        # Identical to the xsec of the dataset (required if normalizing using 1 xsec and sum_mc_weights)
        xsecs={13: tt_dl.get_xsec(13)}
    )

    ttbj = tt_dl.add_process(
        name="ttbj",
        id=tt_dl_id + 51,
        label="ttbj",
        color="#f89c20",
        # Identical to the xsec of the dataset (required if normalizing using 1 xsec and sum_mc_weights)
        xsecs={13: tt_dl.get_xsec(13)}
    )

    ttcc = tt_dl.add_process(
        name="ttcc",
        id=tt_dl_id + 42,
        label="ttcc",
        color="#5790fc",
        # Identical to the xsec of the dataset (required if normalizing using 1 xsec and sum_mc_weights)
        xsecs={13: tt_dl.get_xsec(13)}
    )

    ttcj = tt_dl.add_process(
        name="ttcj",
        id=tt_dl_id + 41,
        label="ttcj",
        color="#7a21dd",
        # Identical to the xsec of the dataset (required if normalizing using 1 xsec and sum_mc_weights)
        xsecs={13: tt_dl.get_xsec(13)}
    )

    ttll = tt_dl.add_process(
        name="ttll",
        id=tt_dl_id + 2,
        label="ttLL",
        color="#964a8b",
        # Identical to the xsec of the dataset (required if normalizing using 1 xsec and sum_mc_weights)
        xsecs={13: tt_dl.get_xsec(13)}
    )

    ttother = tt_dl.add_process(
        name="ttother",
        id=tt_dl_id + 1,
        label="tt_other",
        color="#9c9ca1",
        # Identical to the xsec of the dataset (required if normalizing using 1 xsec and sum_mc_weights)
        xsecs={13: tt_dl.get_xsec(13)}
    )

