# coding: utf-8

"""
Definition of categories.

Categories are assigned a unique integer ID according to a fixed numbering
scheme, with digits/groups of digits indicating the different category groups:

                   lowest digit
                              |
    +---+---+---+---+---+---+---+
    | D | D | B | B | X | T | C |
    +---+---+---+---+---+---+---+

    C = channel       (1: electron [1e], 2: muon [1m])
    T = top-tag       (1: no top-tag [0t], 2: one top-tag [1t])
    X = chi2 cut      (1: pass [chi2pass], 2: fail [chi2fail])
    B = other binning (e.g. abs(cos(theta*)) a.k.a. 'acts')
    D = DNN category

Category groups are defined at different stages in the workflow:

    C, T: added after selection
    X, B: added after production
    D:    added after machine learning step

A digit group consisting entirely of zeroes ('0') represents the inclusive
category for the corresponding category group, i.e. no selection from that
group is applied.

This scheme encodes child/parent relations into the ID, making it easy
to check if categories overlap or are categories of each other. When applied
to a set of leaf categories, the sum of the category IDs is the ID of the
parent category.
"""

import order as od

from columnflow.ml import MLModel
from columnflow.config_util import create_category_combinations, add_category
from ttcc.ml.categories import register_ml_selectors


def name_fn(categories: dict[str, od.Category]):
    """Naming function for automatically generated combined categories."""
    return "__".join(
        cat.name for cat in categories.values()
        if cat.name is not None
    )


def kwargs_fn(categories: dict[str, od.Category]):
    """Customization function for automatically generated combined categories."""
    return {
        "id": sum(cat.id for cat in categories.values()),
        "selection": [cat.selection for cat in categories.values()],
        "label": ", ".join(
            cat.label for cat in categories.values()
        ),
    }



def add_reco_categories(cfg):
    add_category(
        cfg,
        id=111,
        name="cat_2j",
        selection="cat_2j",
        label=r"$\geq 2$ jets",
    )
    add_category(
        cfg,
        id=1,
        name="cat_4j",
        selection="cat_4j",
        label=r"$\geq 4$ jets",
    )
    add_category(
        cfg,
        name="cat_ee",
        selection="cat_ee",
        label=r"ee channel",
    )
    add_category(
        cfg,
        name="cat_emu",
        selection="cat_emu",
        label=r"e\mu channel",
    )
    add_category(
        cfg,
        name="cat_mumu",
        selection="cat_mumu",
        label=r"\mu\mu channel",
    )
    add_category(
        cfg,
        name="cat_4hf",
        selection="cat_4hf",
        label=r"$\geq 4$ b/c-tags (L)",
    )
    add_category(
        cfg,
        id=10,
        name="cat_4b",
        selection="cat_4b",
        label=r"$\geq 4$ b-tags (L)",
    )
    add_category(
        cfg,
        name="cat_3b",
        selection="cat_3b",
        label=r"3 b-tags (L)",
    )
    add_category(
        cfg,
        name="cat_2c",
        selection = "cat_2c",
        label=r"2 b-tags (L), $\geq 2$ c-tags (L)",
    )
    add_category(
        cfg,
        name="cat_1c",
        selection = "cat_1c",
        label=r"2 b-tags (L), 1 c-tag (L)",
    )


def add_gen_categories(cfg):
    #---------------------
    #Gen Categories
    #---------------------
    # TODO clean up
    add_category(
        cfg,
        name="Gen_G4b",
        selection = "Gen_G4b",
        label="baseline & >=4 gen b jets",
    )
    add_category(
        cfg,
        name="Gen_G3b",
        selection = "Gen_G3b",
        label="baseline & >=3 gen b jets",
    )
    add_category(
        cfg,
        name="Gen_G2b",
        selection = "Gen_G2b",
        label="baseline & >=2 gen b jets",
    )
    add_category(
        cfg,
        name="Gen_G2c",
        selection = "Gen_G2c",
        label="baseline & >=2 gen c jets",
    )
    add_category(
        cfg,
        name="Gen_G2bG2c",
        selection = "Gen_G2bG2c",
        label="baseline & >=2 gen b jets\\ & >=2 gen c jets",
    )


