# coding: utf-8

"""
Configuration of the ttcc analysis.
"""

import functools

import law
import order as od
from scinum import Number

from columnflow.util import DotDict, maybe_import
from columnflow.columnar_util import EMPTY_FLOAT, ColumnCollection
from columnflow.config_util import (
    get_root_processes_from_campaign, get_shifts_from_sources, 
    verify_config_processes,
)
from ttcc.util import four_vec
from columnflow.config_util import create_category_combinations
from ttcc.config.processes import add_ttcc_processes
from ttcc.config.datasets import add_ttcc_datasets, configure_ttcc_datasets

ak = maybe_import("awkward")


#
# the main analysis object
#

analysis_ttcc = ana = od.Analysis(
    name="analysis_ttcc",
    id=1,
)

# analysis-global versions
# (see cfg.x.versions below for more info)
ana.x.versions = {}

# files of bash sandboxes that might be required by remote tasks
# (used in cf.HTCondorWorkflow)
ana.x.bash_sandboxes = ["$CF_BASE/sandboxes/cf.sh"]
default_sandbox = law.Sandbox.new(law.config.get("analysis", "default_columnar_sandbox"))
if default_sandbox.sandbox_type == "bash" and default_sandbox.name not in ana.x.bash_sandboxes:
    ana.x.bash_sandboxes.append(default_sandbox.name)

# files of cmssw sandboxes that might be required by remote tasks
# (used in cf.HTCondorWorkflow)
ana.x.cmssw_sandboxes = [
    "$CF_BASE/sandboxes/cmssw_default.sh",
]

# config groups for conveniently looping over certain configs
# (used in wrapper_factory)
ana.x.config_groups = {}


#
# setup configs
#

# an example config is setup below, based on cms NanoAOD v9 for Run2 2017, focussing on
# ttbar and single top MCs, plus single muon data
# update this config or add additional ones to accomodate the needs of your analysis
# TODO choose year by command line argument?
from cmsdb.campaigns.run2_2018_nano_v9 import campaign_run2_2018_nano_v9
# from cmsdb.campaigns.run2_2017_nano_v9 import campaign_run2_2017_nano_v9
# copy the campaign
# (creates copies of all linked datasets, processes, etc. to allow for encapsulated customization)
campaign = campaign_run2_2018_nano_v9.copy()

# get all root processes
procs = get_root_processes_from_campaign(campaign)

# create a config by passing the campaign, so id and name will be identical
cfg = ana.add_config(campaign)

# gather campaign data
year = campaign.x.year

add_ttcc_processes(cfg, campaign)
add_ttcc_datasets(cfg, campaign)
# TODO  command like argument for file limiter?
configure_ttcc_datasets(cfg, limit_dataset_files=2)
#configure_ttcc_datasets(cfg)
    
# verify that the root process of all datasets is part of any of the registered processes
verify_config_processes(cfg, warn=True)

# columns to keep after certain steps
from ttcc.config.keep_columns import keep_columns
cfg.x.keep_columns = keep_columns

# default objects, such as calibrator, selector, producer, ml model, inference model, etc
cfg.x.default_calibrator = "default"
cfg.x.default_selector = "Reco_selection"
cfg.x.default_producer = "Reco_production"
cfg.x.default_ml_model = None
cfg.x.default_inference_model = "example"
cfg.x.default_categories = (
    "cat_4j", "cat_4hf", "cat_4b", "cat_2c"
    )
cfg.x.default_variables = (
    "n_jet", "n_hftagL", "n_hftagM",
    "n_btagL", "n_btagM", "n_btagT", 
    "n_ctagL", "n_ctagM", "n_ctagT", 
    )

# process groups for conveniently looping over certain processs
# (used in wrapper_factory and during plotting)
cfg.x.process_groups = {
    "default": ["ttbb", "ttcc", "ttbj", "ttcj", "ttll", "ttother"]
    }

# dataset groups for conveniently looping over certain datasets
# (used in wrapper_factory and during plotting)
cfg.x.dataset_groups = {}

# category groups for conveniently looping over certain categories
# (used during plotting)
cfg.x.category_groups = {
    "reco": ["cat_4j", "cat_4hf", "cat_4b", "cat_3b", "cat_2c", "cat_1c"],
    "base": ["cat_2j"]
    }

# shift groups for conveniently looping over certain shifts
# (used during plotting)
cfg.x.shift_groups = {}

# selector step groups for conveniently looping over certain steps
# (used in cutflow tasks)
cfg.x.selector_step_groups = {
    "default": ["sel_2l", "sel_4j"],
}

# custom method and sandbox for determining dataset lfns
cfg.x.get_dataset_lfns = None
cfg.x.get_dataset_lfns_sandbox = None

# whether to validate the number of obtained LFNs in GetDatasetLFNs
# (currently set to false because the number of files per dataset is truncated to 2)
cfg.x.validate_dataset_lfns = False

# configure dataset 
from ttcc.config.data_settings import set_lumi, set_triggers, set_minbias, set_metfilters
cfg = set_lumi(cfg, year)
cfg = set_minbias(cfg)
cfg = set_triggers(cfg)
cfg = set_metfilters(cfg)


# configure jecs
from ttcc.config.jec_settings import set_jec, set_jer
# jec configuration
# https://twiki.cern.ch/twiki/bin/view/CMS/JECDataMC?rev=201
jerc_postfix = "APV" if year == 2016 and campaign.x.vfp == "post" else ""
cfg = set_jec(cfg, year, jerc_postfix)
cfg = set_jer(cfg, year, jerc_postfix)


# configure shifts
from ttcc.config.shift_settings import add_shifts
cfg = add_shifts(cfg, year, campaign)

# event weight columns as keys in an OrderedDict, mapped to shift instances they depend on
# TODO whats this
get_shifts = functools.partial(get_shifts_from_sources, cfg)
cfg.x.event_weights = DotDict({
    "normalization_weight": [],
    "muon_weight": get_shifts("mu"),
})

# target file size after MergeReducedEvents in MB
cfg.x.reduced_file_size = 512.0


# versions per task family, either referring to strings or to callables receving the invoking
# task instance and parameters to be passed to the task family
# cfg.x.versions = {
#     # "cf.CalibrateEvents": "prod1",
#     # "cf.SelectEvents": (lambda cls, inst, params: "prod1" if params.get("selector") == "default" else "dev1"),
#     # ...
# }

# versions per task family and optionally also dataset and shift
# None can be used as a key to define a default value
cfg.set_aux("versions", {
})

# TODO Maybe modify channel ids
cfg.add_channel(name="ee", id=1)
cfg.add_channel(name="mumu", id=2)
cfg.add_channel(name="emu", id=3)


# add categories using the "add_category" tool which adds auto-generated ids
# the "selection" entries refer to names of selectors, e.g. in selection/example.py
# note: it is recommended to always add an inclusive category with id=1 or name="incl" which is used
#       in various places, e.g. for the inclusive cutflow plots and the "empty" selector
from ttcc.config.categories import add_reco_categories, add_gen_categories
add_reco_categories(cfg)
#add_gen_categories(cfg)

# def name_fn(categories: dict[str, od.Category]):
#     """Naming function for automatically generated combined categories."""
#     return "__".join(
#         cat.name for cat in categories.values()
#         if cat.name is not None
#     )


# def kwargs_fn(categories: dict[str, od.Category]):
#     """Customization function for automatically generated combined categories."""
#     return {
#         "id": sum(cat.id for cat in categories.values()),
#         "selection": [cat.selection for cat in categories.values()],
#         "label": ", ".join(
#             cat.label for cat in categories.values()
#         ),
#     }

# create_category_combinations(cfg, category_groups, name_fn, kwargs_fn)

# add variables
# (the "event", "run" and "lumi" variables are required for some cutflow plotting task,
# and also correspond to the minimal set of columns that coffea's nano scheme requires)
from ttcc.config.variables import add_default_variables, add_jet_variables, add_lep_variables
def_vars = add_default_variables(cfg)
jet_vars = add_jet_variables(cfg)
lep_vars = add_lep_variables(cfg)

# variable groups for conveniently looping over certain variables
# (used during plotting)
cfg.x.variable_groups = {
    "lep_vars": lep_vars,
    "jet_vars": jet_vars
    }

