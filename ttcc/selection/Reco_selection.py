# coding: utf-8

"""
Exemplary selection methods.
"""

from typing import Tuple

from collections import defaultdict

from columnflow.util import maybe_import
from columnflow.selection import Selector, SelectionResult, selector
from columnflow.selection.stats import increment_stats
from columnflow.selection.util import sorted_indices_from_mask
from columnflow.production.cms.mc_weight import mc_weight
from columnflow.columnar_util import set_ak_column
from ttcc.selection.base_selection import TetraVec, prepare_selection, post_selection

from ttcc.selection.reco_objects import object_selection
from ttcc.selection.gen_objects import gen_object_selection
from ttcc.selection.Gen_selection import dl_gen_selection

np = maybe_import("numpy")
ak = maybe_import("awkward")
coffea = maybe_import("coffea")


@selector(
    uses=(
        four_vec({"Electron", "Muon"}, {"pdgId", "tight"}) |
        {"n_electron_tight", "n_electron_loose", "n_muon_tight", "n_muon_loose"}
    ),
    produces={"n_lep", #"Lepton",
        "Lepton.pt","Lepton.eta","Lepton.phi","Lepton.mass","channel_id"},
)
def dl_lepton_selection(
    self: Selector,
    events: ak.Array,
    results: SelectionResult,
    **kwargs,
) -> tuple[ak.Array, SelectionResult]:
    
    # get the already preselected e/mu
    electron = (events.Electron[results.objects.Electron.Electron])
    muon = (events.Muon[results.objects.Muon.Muon])

    # create new object: leptons
    lepton = ak.concatenate([muon, electron], axis=-1)
    lepton = lepton[ak.argsort(lepton.pt, axis=-1, ascending=False)]

    # pad leptons up to two for z resonance
    fill_with = {"pt": -999, "eta": -999, "phi": -999, "mass": -999, 
        "pdgId": -999, "tight": False}
    lepton = ak.fill_none(ak.pad_none(lepton, 2, axis=-1), fill_with)

    # Z boson reconstruction
    z_comb = ak.combinations(lepton, 2, axis=1)
    _lep1, _lep2 = ak.unzip(z_comb)

    # pt mask
    lep_pt_mask = (
        (lepton.pt[:, 0] > 25) &
        (lepton.pt[:, 1] > 15)
    )

    # calc mll for each 2l comb
    mll = (TetraVec(_lep1) + TetraVec(_lep2)).mass
    
    # OSSF pairs on Z peak or with low mll
    z_masks = (
        (_lep1.pdgId == -_lep2.pdgId) &
        ((abs(mll - 91) < 15) | (mll < 12))
    )
    z_mask = ak.any(z_masks, axis=-1)

    # remove padding again
    lepton = lepton[lepton.pdgId != -999]
        
    # lepton selection mask
    n_tight_e = events.n_electron_tight
    n_loose_e = events.n_electron_loose
    n_tight_m = events.n_muon_tight
    n_loose_m = events.n_muon_loose

    # base selection - no z, no loose lep
    sel_base = (
        (~z_mask) & # veto z resonance
        (lep_pt_mask) & 
        (n_tight_m == n_loose_m) & 
        (n_tight_e == n_loose_e) & 
        (ak.all(lepton.tight, axis=-1))
    )
    sel_2mu = (sel_base) & (n_tight_m == 2) & (n_tight_e == 0) 
    sel_2el = (sel_base) & (n_tight_e == 2) & (n_tight_m == 0)
    sel_emu = (sel_base) & (n_tight_e == 1) & (n_tight_m == 1)
    sel_2l = sel_2mu | sel_2el | sel_emu

    #Create and save channel id
    channel_id = ak.zeros_like(events.event, dtype=np.int8)
    channel_id = ak.where((sel_2mu), self.config_inst.get_channel("mumu").id,channel_id)
    channel_id = ak.where((sel_2el), self.config_inst.get_channel("ee").id,channel_id)
    channel_id = ak.where((sel_emu), self.config_inst.get_channel("emu").id,channel_id)
    events = set_ak_column(events, "channel_id", channel_id)

    # save counter columns
    events = set_ak_column(events, "n_lep", n_tight_e + n_tight_m)
    events = set_ak_column(events, "Lepton.pt", lepton.pt)
    events = set_ak_column(events, "Lepton.eta", lepton.eta)
    events = set_ak_column(events, "Lepton.phi", lepton.phi)
    events = set_ak_column(events, "Lepton.mass", lepton.mass)
    
    

    return events, SelectionResult(
        steps={
            "sel_2l":  sel_2l, 
            "sel_2mu": sel_2mu, 
            "sel_2el": sel_2el, 
            "sel_emu": sel_emu, 
        },
    )



@selector(
    uses=four_vec({"Electron", "Muon"}),
    produces={"n_jet"}    
)
def jet_selection(
    self: Selector,
    events: ak.Array,
    results: SelectionResult,
    **kwargs,
) -> tuple[ak.Array, SelectionResult]:

    # get pre cleaned jet
    jet = (events.Jet[results.objects.Jet.Jet])

    # jet multiplicity column
    n_jet = ak.num(jet, axis=-1)
    events = set_ak_column(events, "n_jet", n_jet)
    
    # different jet selections
    jet_sel_4 = (n_jet >= 4) 
    jet_sel_3 = (n_jet >= 3) 
    jet_sel_2 = (n_jet >= 2)

    # build and return selection results
    # "objects" maps source columns to new columns and selections to be applied on the old columns
    # to create them, e.g. {"Jet": {"MyCustomJetCollection": indices_applied_to_Jet}}
    return events, SelectionResult(
        steps={
            "sel_4j": jet_sel_4,#"bjet": bjet_sel,
            "sel_3j": jet_sel_3,#"bjet": bjet_sel,
            "sel_2j": jet_sel_2,#"bjet": bjet_sel,
        },
    )




@selector(
    uses={
        # selectors / producers called within _this_ selector
        prepare_selection, object_selection, dl_lepton_selection, jet_selection, post_selection,
        gen_object_selection, dl_gen_selection
    },
    produces={
        # selectors / producers whose newly created columns should be kept
        prepare_selection, object_selection, dl_lepton_selection, jet_selection, post_selection,
        gen_object_selection, dl_gen_selection
    },
    exposed=True,
)
def Reco_selection(
    self: Selector,
    events: ak.Array,
    stats: defaultdict,
    **kwargs,
) -> tuple[ak.Array, SelectionResult]:

    # prepare selections by adding mc weights and proc ids
    events, results = self[prepare_selection](events, stats, **kwargs)
    #events = self[custom_process_ids](events, **kwargs)

    # apply trigger selection
    #events, trigger_results = self[trigger_selection](events, **kwargs)
    #results += trigger_results

    # apply object selections
    events, object_results = self[object_selection](events, stats, **kwargs)
    results += object_results

    # apply dilepton event selection
    events, lepton_results = self[dl_lepton_selection](events, results, **kwargs)
    results += lepton_results

    #only do things on MC
    if self.dataset_inst.is_mc:
        # apply gen object defitionns
        events, genobj_results = self[gen_object_selection](events, stats, **kwargs)
        results += genobj_results

        # gen dilepton selection
        events, genlep_results = self[dl_gen_selection](events, results, **kwargs)
        results += genlep_results

    # jet selection
    events, jet_results = self[jet_selection](events, results, **kwargs)
    results += jet_results

    # combined event selection after all steps
    #results.event = results.steps.sel_trig & results.steps.sel_2l & results.steps.sel_2j
    results.event = results.steps.sel_2l & results.steps.sel_2j

    # post selection steups
    events, results = self[post_selection](events, results, stats, **kwargs)
    return events, results
