from collections import defaultdict
from typing import Tuple

import law

from columnflow.util import maybe_import
from columnflow.columnar_util import set_ak_column
from columnflow.selection import Selector, SelectionResult, selector

np = maybe_import("numpy")
ak = maybe_import("awkward")
coffea = maybe_import("coffea")

@selector(
    uses=(
        "process_id",
        "n_genjet", "n_genbjet", "n_gencjet",
        "n_genlep"
    ),
    produces={"process_id"},
    mc_only=True,
    exposed=False
)
def def_ttbar_processes(
    self: Selector, events: ak.Array, **kwargs
) -> Tuple[ak.Array, SelectionResult]:

    print("ttbar procs", self.dataset_inst.processes)

    # counters
    n_GenJet = (events.n_genjet)
    n_GenbJet = (events.n_genbjet)
    n_GencJet = (events.n_gencjet)
    n_GenlJet = (events.n_genljet)
    n_GenLep = (events.n_genlep)

    # subprocess definitions
    base_mask = (n_GenJet >= 4) & (n_GenLep == 2)
    ttbb_mask = (base_mask) & (n_GenbJet >= 4)
    ttbj_mask = (base_mask) & (n_GenbJet == 3)
    ttcc_mask = (base_mask) & (n_GenbJet == 2) & (n_GencJet >= 2)
    ttcj_mask = (base_mask) & (n_GenbJet == 2) & (n_GencJet == 1)
    ttLL_mask = (base_mask) & (n_GenbJet == 2) & (n_GencJet == 0)
    # two alternative definitions for the remainder
    # ttot_mask = (~ttbb_mask) & (~ttbj_mask) & (~ttcc_mask) & (~ttcj_mask) & (~ttLL_mask)
    ttot_mask = (n_GenJet < 4) | (n_GenbJet < 2) | (n_GenLep != 2)

    # define new process ids
    process_id = self.config_inst.get_process("tt_dl").id
    process_id = ak.where(ttbb_mask, (process_id + 52), (process_id)) # 5X -> b, 2 -> 2b
    process_id = ak.where(ttbj_mask, (process_id + 51), (process_id)) # 5X -> b, 1 -> 1b
    process_id = ak.where(ttcc_mask, (process_id + 42), (process_id)) # 4X -> c, 2 -> 2c
    process_id = ak.where(ttcj_mask, (process_id + 41), (process_id)) # 4X -> c, 1 -> 1c
    process_id = ak.where(ttLL_mask, (process_id + 2), (process_id)) # 0X -> L, 2 -> 2L
    process_id = ak.where(ttot_mask, (process_id + 1), (process_id)) # 0X -> L, 1 -> other

    events = set_ak_column(events, "process_id", process_id)   
    return events

@selector(
    uses={def_ttbar_processes},
    produces={def_ttbar_processes},
    mc_only=True,
    exposed=True,
)
def custom_process_ids(
    self: Selector,
    events: ak.Array,
    **kwargs,
) -> ak.Array:

    if self.dataset_inst.name == "tt_dl_powheg":
        events = self[def_ttbar_processes](events, **kwargs)
    return events
