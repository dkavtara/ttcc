

# coding: utf-8

"""
Selection modules for object selection of Muon, Electron, and Jet.
https://gitlab.cern.ch/ghentanalysis/columnflowanalysis/ttz/-/blob/master/ttz/selection/objects.py
"""

from collections import defaultdict
from typing import Tuple

import law

from columnflow.util import maybe_import
from columnflow.columnar_util import set_ak_column
from columnflow.production.util import attach_coffea_behavior
from columnflow.selection import Selector, SelectionResult, selector
from ttcc.util import four_vec
from ttcc.selection.util import masked_sorted_indices

ak = maybe_import("awkward")


def masked_sorted_indices(mask: ak.Array, sort_var: ak.Array, ascending: bool = False) -> ak.Array:
    """
    Helper function to obtain the correct indices of an object mask
    """
    indices = ak.argsort(sort_var, axis=-1, ascending=ascending)
    return indices[mask[indices]]


@selector(
    uses=four_vec(
        ("Muon"),
        ("pfRelIso04_all", "tightId")
    ) | {"event"},
    produces={"Muon.tight", "n_muon_loose", "n_muon_tight"},
    triggers=None
)
def muon_object(
        self: Selector,
        events: ak.Array,
        stats: defaultdict,
        **kwargs,
) -> Tuple[ak.Array, SelectionResult]:

    muon = (events.Muon)

    # loose object electron mask
    mu_mask_loose = (
        (abs(muon.eta) < 2.4) &
        (muon.pt > 15.) &
        (muon.pfRelIso04_all < 0.25)
    )
    mu_mask_tight = (
        (mu_mask_loose) &
        (muon.tightId) 
    )
    
    events = set_ak_column(events, "Muon.tight", mu_mask_tight, value_type=bool)
    events = set_ak_column(events, "n_muon_tight", ak.sum(mu_mask_tight, axis=-1))
    events = set_ak_column(events, "n_muon_loose", ak.sum(mu_mask_loose, axis=-1))

    return events, SelectionResult(
        steps={},
        objects={
            "Muon": {
                "Muon": masked_sorted_indices(mu_mask_tight, muon.pt)
            }
        },
    )


@selector(
    uses=four_vec(
        ("Electron"),
        ("dxy", "dz", "deltaEtaSC", "cutBased", 
        )
    ) | four_vec(
        ("Muon"),
    ),
    produces={"Electron.tight", "n_electron_tight", "n_electron_loose"},
    triggers=None
)
def electron_object(
        self: Selector,
        events: ak.Array,
        results: SelectionResult,
        stats: defaultdict,
        **kwargs,
) -> Tuple[ak.Array, SelectionResult]:

    electron = (events.Electron)

    # eta super cluster
    absEtaSC = abs(electron.eta + electron.deltaEtaSC)

    # in transition region for veto
    in_hole = (absEtaSC <= 1.5660) & (absEtaSC >= 1.4442)

    # dxyz cuts for barrel and endcap
    barrel_sel = (absEtaSC <= 1.479) & (abs(electron.dxy) < 0.05) & (abs(electron.dz) < 0.1) 
    endcap_sel = (absEtaSC > 1.479) & (abs(electron.dxy) < 0.1) & (abs(electron.dz) < 0.2) 

    e_mask_loose = (
        (electron.cutBased >= 1) &
        (electron.pt > 15) &
        (abs(electron.eta) < 2.5)   
    )   
    e_mask_tight = (
        (e_mask_loose) &
        (electron.cutBased == 4) &
        (~in_hole) &
        ( barrel_sel | endcap_sel ) 
    )        

    events = set_ak_column(events, "Electron.tight", e_mask_tight, value_type=bool)
    events = set_ak_column(events, "n_electron_tight", ak.sum(e_mask_tight, axis=-1))
    events = set_ak_column(events, "n_electron_loose", ak.sum(e_mask_loose, axis=-1))

    return events, SelectionResult(
        steps={},
        objects={
            "Electron": {
                "Electron": masked_sorted_indices(e_mask_tight, electron.pt)
            }
        },
    )


@selector(
    uses=(
        four_vec({"Electron", "Muon"}) | 
        four_vec("Jet", ("jetId", "hadronFlavour"))),
    exposed=False,
)
def jet_object(
    self: Selector,
    events: ak.Array,
    results: SelectionResult,
    stats: defaultdict,
    **kwargs,
) -> Tuple[ak.Array, SelectionResult]:

    # collections
    jet = (events.Jet)
    muon = (events.Muon)[results.objects.Muon.Muon]
    electron = (events.Electron)[results.objects.Electron.Electron]

    # overlap veto with leptons
    dR_mask = (
        (ak.is_none(jet.nearest(muon, threshold=0.4), axis=-1)) &
        (ak.is_none(jet.nearest(electron, threshold=0.4), axis=-1))
    )

    # jet selection
    jet_mask = (
        (jet.pt > 25) &
        (abs(jet.eta) < 2.5) &
        (jet.jetId >= 2) &
        (dR_mask)
    )

    jet_indices = masked_sorted_indices(jet_mask, events.Jet.pt)
    n_jets = ak.sum(jet_mask, axis=-1)

    # genhfhadron matcher jet truth flavors
    bjet_indices = masked_sorted_indices(jet_mask & (events.Jet.hadronFlavour == 5), events.Jet.pt)
    cjet_indices = masked_sorted_indices(jet_mask & (events.Jet.hadronFlavour == 4), events.Jet.pt)
    ljet_indices = masked_sorted_indices(jet_mask & (events.Jet.hadronFlavour == 0), events.Jet.pt)

    return events, SelectionResult(
        steps={},
        objects={
            "Jet": {
                "Jet": jet_indices,
                "bJet": bjet_indices,
                "cJet": cjet_indices,
                "lJet": ljet_indices,
            },
        },
        aux={
            "jet_mask": jet_mask,
            "n_jets": n_jets,
        },
    )


@selector(
    uses=(muon_object, electron_object, jet_object),
    exposed=False
)
def object_selection(
    self: Selector,
    events: ak.Array,
    stats: defaultdict,
    **kwargs,
) -> Tuple[ak.Array, SelectionResult]:
    # apply muon object selection
    events, results = self[muon_object](events, stats, **kwargs)

    # apply electron object selection
    events, electron_results = self[electron_object](events, results, stats, **kwargs)
    results += electron_results

    # apply jet object selection
    events, jet_results = self[jet_object](events, results, stats, **kwargs)
    results += jet_results

    return events, results

