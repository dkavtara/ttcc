# coding: utf-8

"""
Selection modules for gen object selection of Muon, Electron, and Jet.
https://gitlab.cern.ch/ghentanalysis/columnflowanalysis/ttz/-/blob/master/ttz/selection/objects.py
"""

from collections import defaultdict
from typing import Tuple

import law

from columnflow.util import maybe_import, four_vec
from columnflow.columnar_util import set_ak_column
from columnflow.selection import Selector, SelectionResult, selector
from columnflow.selection.util import masked_sorted_indices

ak = maybe_import("awkward")

@selector(
    uses=four_vec(
        ("GenDressedLepton"),
        ("pdgId")
    ) | {"event"},
    produces={
        "GenDressedLepton.mu", 
        "GenDressedLepton.el", 
        "GenDressedLepton.lep", 
        "n_genmu", "n_genel", "n_genlep"
    }
)
def genlep_object(
        self: Selector,
        events: ak.Array,
        stats: defaultdict,
        **kwargs,
) -> Tuple[ak.Array, SelectionResult]:

    genlep = (events.GenDressedLepton)

    # loose object electron mask
    mu_mask_gen = (
        (abs(genlep.pdgId) == 13) &
        (abs(genlep.eta) < 2.4) &
        (genlep.pt > 15.)
    )
    el_mask_gen = (
        (abs(genlep.pdgId) == 11) &
        (abs(genlep.eta) < 2.4) &
        (genlep.pt > 15.)
    )
    lep_mask_gen = (mu_mask_gen) | (el_mask_gen) 

    events = set_ak_column(events, "GenDressedLepton.el", el_mask_gen, value_type=bool)
    events = set_ak_column(events, "GenDressedLepton.mu", mu_mask_gen, value_type=bool)
    events = set_ak_column(events, "GenDressedLepton.lep", lep_mask_gen, value_type=bool)
    events = set_ak_column(events, "n_genmu", ak.sum(mu_mask_gen, axis=-1))
    events = set_ak_column(events, "n_genel", ak.sum(el_mask_gen, axis=-1))
    events = set_ak_column(events, "n_genlep", ak.sum(lep_mask_gen, axis=-1))

    return events, SelectionResult(
        steps={},
        objects={
            "GenDressedLepton": {
                "GenMuon": masked_sorted_indices(mu_mask_gen, genlep.pt),
                "GenElectron": masked_sorted_indices(el_mask_gen, genlep.pt),
                "GenLepton": masked_sorted_indices(lep_mask_gen, genlep.pt)
            }
        },
    )


@selector(
    uses=(
        four_vec({"GenDressedLepton"}) | 
        four_vec("GenJet", ("hadronFlavour"))),
    #produces={
    #},
    exposed=False,
)
def genjet_object(
    self: Selector,
    events: ak.Array,
    results: SelectionResult,
    stats: defaultdict,
    **kwargs,
) -> Tuple[ak.Array, SelectionResult]:

    # collections
    jet = (events.GenJet)
    lep = (events.GenDressedLepton)[results.objects.GenDressedLepton.GenLepton]

    # overlap veto with leptons
    dR_mask = (ak.is_none(jet.nearest(lep, threshold=0.4), axis=-1))
    
    # jet selection
    jet_mask = (
        (jet.pt > 20) &
        (abs(jet.eta) < 2.5) &
        (dR_mask)
    )

    # genhfhadron matcher jet truth flavors
    bjet_mask = jet_mask & (events.GenJet.hadronFlavour == 5)
    cjet_mask = jet_mask & (events.GenJet.hadronFlavour == 4)
    ljet_mask = jet_mask & (events.GenJet.hadronFlavour == 0)

    # indices
    jet_indices = masked_sorted_indices(jet_mask, events.GenJet.pt)
    bjet_indices = masked_sorted_indices(bjet_mask, events.GenJet.pt)
    cjet_indices = masked_sorted_indices(cjet_mask, events.GenJet.pt)
    ljet_indices = masked_sorted_indices(ljet_mask, events.GenJet.pt)

    #events = set_ak_column(events, "n_genjet", ak.sum(jet_mask, axis=-1))
    #events = set_ak_column(events, "n_genbjet", ak.sum(bjet_mask, axis=-1))
    #events = set_ak_column(events, "n_gencjet", ak.sum(cjet_mask, axis=-1))
    #events = set_ak_column(events, "n_genljet", ak.sum(ljet_mask, axis=-1))

    return events, SelectionResult(
        steps={},
        objects={
            "GenJet": {
                "GenJet": jet_mask,
                "GenbJet": bjet_mask,
                "GencJet": cjet_mask,
                "GenlJet": ljet_mask,
            }
        },
    )


@selector(
    uses=(genlep_object, genjet_object),
    exposed=False
)
def gen_object_selection(
    self: Selector,
    events: ak.Array,
    stats: defaultdict,
    **kwargs,
) -> Tuple[ak.Array, SelectionResult]:

    # apply gen lepton object selection
    events, results = self[genlep_object](events, stats, **kwargs)

    # apply gen jet object selection
    events, jet_results = self[genjet_object](events, results, stats, **kwargs)
    results += jet_results

    return events, results


