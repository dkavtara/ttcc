# coding: utf-8

"""
Exemplary selection methods.
"""

from typing import Tuple

from collections import defaultdict

from columnflow.util import maybe_import, four_vec
from columnflow.selection import Selector, SelectionResult, selector
from columnflow.selection.stats import increment_stats
from columnflow.selection.util import sorted_indices_from_mask
from columnflow.production.util import attach_coffea_behavior
from columnflow.production.processes import process_ids
from columnflow.production.cms.mc_weight import mc_weight
from ttcc.production.cutflow import cutflow_features



np = maybe_import("numpy")
ak = maybe_import("awkward")
coffea = maybe_import("coffea")


def TetraVec(arr: ak.Array) -> ak.Array:
    TetraVec = ak.zip({"pt": arr.pt, "eta": arr.eta, "phi": arr.phi, "mass": arr.mass},
    with_name="PtEtaPhiMLorentzVector",
    behavior=coffea.nanoevents.methods.vector.behavior)
    return TetraVec

@selector(
    uses={
        process_ids, attach_coffea_behavior,
        mc_weight
    },
    produces={
        process_ids, attach_coffea_behavior,
        mc_weight
    },
    exposed=False,
)
def prepare_selection(
    self: Selector,
    events: ak.Array,
    stats: defaultdict,
    **kwargs,
) -> Tuple[ak.Array, SelectionResult]:

    if self.dataset_inst.is_mc:
        events = self[mc_weight](events, **kwargs)

    # create process ids
    events = self[process_ids](events, **kwargs)
    # ensure coffea behavior
    events = self[attach_coffea_behavior](events, **kwargs)

    results = SelectionResult()
    return events, results





@selector(
    uses={cutflow_features, increment_stats},
    produces={cutflow_features},
    exposed=False
)
def post_selection(
    self: Selector,
    events: ak.Array,
    results: SelectionResult,
    stats: defaultdict,
    **kwargs,
) -> Tuple[ak.Array, SelectionResult]:
    # TODO build categories
    #events = self[category_ids](events, results=results, **kwargs)

    # cutflow features
    # TODO what is done here currently? Do we need that?
    events = self[cutflow_features](events, results.objects, **kwargs)

    # increment stats
    weight_map = {
        "num_events": Ellipsis,
        "num_events_selected": results.event,
    }
    group_map = {}
    if self.dataset_inst.is_mc:
        weight_map = {
            **weight_map,
            # mc weight for all events
            "sum_mc_weight": (events.mc_weight, Ellipsis),
            "sum_mc_weight_selected": (events.mc_weight, results.event),
        }
        group_map = {
            # per process
            "process": {
                "values": events.process_id,
                "mask_fn": (lambda v: events.process_id == v),
            },
        }
    events, results = self[increment_stats](
        events,
        results,
        stats,
        weight_map=weight_map,
        group_map=group_map,
        **kwargs,
    )

    return events, results

