# coding: utf-8

"""
Exemplary selection methods.
"""

from typing import Tuple

from collections import defaultdict

from columnflow.util import maybe_import, four_vec
from columnflow.selection import Selector, SelectionResult, selector
from columnflow.selection.stats import increment_stats
from columnflow.selection.util import sorted_indices_from_mask
from columnflow.columnar_util import set_ak_column
from ttcc.selection.base_selection import TetraVec, prepare_selection, post_selection
from ttcc.selection.gen_objects import gen_object_selection

np = maybe_import("numpy")
ak = maybe_import("awkward")
coffea = maybe_import("coffea")



@selector(
    uses=(
        four_vec({"GenDressedLepton"}, {"pdgId"}) |
        {"n_genel", "n_genmu"}
    ),
    produces={"n_genlep"}
)
def dl_gen_selection(
    self: Selector,
    events: ak.Array,
    results: SelectionResult,
    **kwargs,
) -> tuple[ak.Array, SelectionResult]:
    # get the already preselected e/mu
    genlep = (events.GenDressedLepton[results.objects.GenDressedLepton.GenLepton])
    # pad leptons up to two for z resonance
    fill_with = {"pt": -999, "eta": -999, "phi": -999, "mass": -999, "pdgId": -999}
    genlep = ak.fill_none(ak.pad_none(genlep, 2, axis=-1), fill_with)

    # Z boson reconstruction
    z_comb = ak.combinations(genlep, 2, axis=1)
    _lep1, _lep2 = ak.unzip(z_comb)

    # pt mask
    lep_pt_mask = (
        (genlep.pt[:, 0] > 25) &
        (genlep.pt[:, 1] > 15)
    )

    # calc mll for each 2l comb
    mll = (TetraVec(_lep1) + TetraVec(_lep2)).mass
    
    # OSSF pairs on Z peak or with low mll
    z_masks = (
        (_lep1.pdgId == -_lep2.pdgId) &
        ((abs(mll - 91) < 15) | (mll < 12))
    )
    z_mask = ak.any(z_masks, axis=-1)
    # remove padding again
    genlep = genlep[genlep.pdgId != -999]
        
    # lepton selection mask
    n_tight_e = events.n_genel
    n_tight_m = events.n_genmu

    # base selection - no z, no loose lep
    sel_base = (
        (~z_mask) & # veto z resonance
        (lep_pt_mask)
    )
    sel_2mu = (sel_base) & (n_tight_m == 2) & (n_tight_e == 0) 
    sel_2el = (sel_base) & (n_tight_e == 2) & (n_tight_m == 0)
    sel_emu = (sel_base) & (n_tight_e == 1) & (n_tight_m == 1)
    sel_2l = sel_2mu | sel_2el | sel_emu
    # save counter columns

    events = set_ak_column(events, "n_genlep", n_tight_e + n_tight_m)

    return events, SelectionResult(
        steps={
            "sel_gen_2l":  sel_2l, 
            "sel_gen_2mu": sel_2mu, 
            "sel_gen_2el": sel_2el, 
            "sel_gen_emu": sel_emu, 
        },
    )


@selector(
    uses={
        prepare_selection, gen_object_selection, dl_gen_selection, post_selection
    },
    # selectors/producers whose newly created columns should be kept!
    produces={
        prepare_selection, gen_object_selection, dl_gen_selection, post_selection
    },
    exposed=True,
)
def Gen_selection(self: Selector, events: ak.Array, stats: defaultdict, **kwargs) -> tuple[ak.Array,SelectionResult]:
    # prepare selections by adding mc weights and proc ids
    events, results = self[prepare_selection](events, stats, **kwargs)

    #only do things on MC
    if self.dataset_inst.is_mc:
        # apply gen object defitionns
        events, genobj_results = self[gen_object_selection](events, stats, **kwargs)
        results += genobj_results
        # gen dilepton selection
        events, genlep_results = self[dl_gen_selection](events, results, **kwargs)
        results += genlep_results

    # post selection steups
    events, results = self[post_selection](events, results, stats, **kwargs)

    return events, results



