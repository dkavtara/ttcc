#!/bin/sh
# dedicated version for test
version="test_2"

# common args for all tasks
args=(
    --version $version
    --dataset
        tt_dl
    --remove-output 0,a,y
)

# test SelectEvents
law run cf.SelectEventsWrapper \
    "${args[@]}"