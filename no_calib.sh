#!/usr/bin/env bash

#
# commands for testing different parts of the workflow
#

# dedicated version for test
version="Gen5"

# common args for all tasks
args=(
    --version $version
    --remove-output 0,a,y
)

# run CalibrateEvents only if there is no calibration
law run cf.CalibrateEventsWrapper \
    --version $version

# test SelectEvents
law run cf.SelectEventsWrapper \
    "${args[@]}"

# test PlotCutflow
plot_cf_args=(
    --version $version
    --categories incl,ttbb
    --processes
        ttbb,ttcc,ttbj,ttcj,ttll,ttother
    --shape-norm True
    --view-cmd echo
    --remove-output 0,a,y
)
echo law run cf.PlotCutflow \
    "${plot_cf_args[@]}"

# # test PlotCutflowVariables
plot_cf_vars_args=(
    --version $version
    --variables cf_n_toptag
    --categories incl,1e,1e__0t,1e__1t
    --processes
        tt_dl
    --dataset
        tt_dl
    --process-settings
        "tt_dl,color1=#000000"
    --hide-errors
    # --shape-norm True
    --view-cmd echo
    --remove-output 0,a,y
)
echo run cf.PlotCutflowVariables1D \
    "${plot_cf_vars_args[@]}"

# test ReduceEvents
law run cf.ReduceEventsWrapper \
    "${args[@]}"

# test MergeReduceEvents
law run cf.MergeReducedEventsWrapper \
    "${args[@]}"

# test ProduceColumns
law run cf.ProduceColumnsWrapper \
    "${args[@]}"

# test PlotVariables1D
plot_args=(
    --version $version
    --variables n_jet,n_bjet,Gen_n_Jet,Gen_n_BJet,Gen_n_CJet,Gen_n_LightJet,
    --categories all,incl,ttbb,cat_2j 
    --processes
      ttbb,ttcc,ttll,ttbj,ttcj,ttother 
    # --dataset
    #     tt_dl
    # --process-settings
    #     "tt_dl,color1=#000000"
    --hide-errors
    # --shape-norm True
    --view-cmd echo
    --remove-output 0,a,y
)
law run cf.PlotVariables1D \
    "${plot_args[@]}"
