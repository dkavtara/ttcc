#!/usr/bin/env bash

#
# plot the distribution of gen-level variables
# and the 2D distributions of matched reco and gen
# variable pairs
#

# law args
args=(
    --version test_gen_studies
    --categories all,Gen_ttbb,Gen_ttcc,Gen_ttll,Gen_ttbj,Gen_ttother
    --selector Gen_selection
    --workers 8
    # --workflow htcondor
    $@
)

# 2D gen-reco plots
# for process in tt zprime_tt_m1000_w100; do
law run cf.PlotVariables2D \
        --variables n_GenJet-n_jet,n_GenBJet-n_bjet \
        --processes tt_dl \
        "${args[@]}"
# done

# 1D gen plots
law run cf.PlotVariables1D \
    --skip-ratio \
    --hide-errors \
    --processes \
        tt_dl \
    --variables \
        n_GenJet,n_GenBJet,n_GenCJet \
    "${args[@]}"